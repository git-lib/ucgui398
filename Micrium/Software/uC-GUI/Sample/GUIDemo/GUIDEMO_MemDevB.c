/*
*********************************************************************************************************
*                                                uC/GUI
*                        Universal graphic software for embedded applications
*
*                       (c) Copyright 2002, Micrium Inc., Weston, FL
*                       (c) Copyright 2002, SEGGER Microcontroller Systeme GmbH
*
*              礐/GUI is protected by international copyright laws. Knowledge of the
*              source code may not be used to write a similar product. This file may
*              only be used in accordance with a license and should not be redistributed
*              in any way. We appreciate your understanding and fairness.
*
----------------------------------------------------------------------
File        : GUIDEMO_MemDevB.c
Purpose     : Memory device demo
----------------------------------------------------------------------
*/

#include "GUI.H"
#include "GUIDEMO.H"
#include "WM.h"
#include "stdio.h"

#if GUI_SUPPORT_MEMDEV

// **************************************************************************
// 内部数据

static const GUI_POINT _aArrow[] = {
  {  0, 0 + 85},
  {-40, -35 + 85},
  {-10, -25 + 85},
  {-10, -85 + 85},
  { 10, -85 + 85},
  { 10, -25 + 85},
  { 40, -35 + 85}
};

//! \brief      绘制上下文结构体
//! \details    包括绘制角度等信息，由回调函数使用
typedef struct {
    int Angle;      //!< 角度值
    int DoClear;    //!< 是否需要清除屏幕
} tDrawContext;

// **************************************************************************
// 内部函数

//! \brief      用户自定医德回调函数
//! \param[in]  p   回调函数参数指针
//! \return     void
static void _Draw(void * p) {
    int XSize = LCD_GetXSize();
    int YSize = LCD_GetYSize();
    tDrawContext * pContext = (tDrawContext *)p;
    int i = pContext->Angle;
    static int iLast = -1;
    static GUI_POINT aPoint[7];
    if (pContext->DoClear)
        GUI_Clear();
    GUI_SetFont(&GUI_FontD24x32);
    GUI_SetTextMode(GUI_TM_TRANS);
    GUI_SetColor(GUI_GRAY);
    GUI_SetFont(&GUI_FontComic18B_1);
    GUI_DispStringHCenterAt("Below arrow", XSize / 2, YSize / 2 - 20);
    if (iLast != i) {
        float Angle = 0.02 * (float)i;
        iLast = i;
        GUI_RotatePolygon(aPoint, _aArrow, 7, Angle);
    }
    GUI_SetColor(GUI_WHITE);
    GUI_FillPolygon(&aPoint[0], 7, XSize / 2, YSize / 2 + 30);
    GUI_SetTextMode(GUI_TM_TRANS);
    GUI_SetColor(GUI_DARKGRAY);
    GUI_SetFont(&GUI_FontComic18B_1);
    GUI_DispStringHCenterAt("Above arrow", XSize / 2, YSize / 2);
}

//! \brief      内存绘图示例程序
//! \return     void
void GUIDEMO_ShowMemoryDevice(void) {
    int i, tDiff, t0;
    tDrawContext DrawContext;
    GUI_COLOR colorBack;
    GUI_RECT rView, rPrev, rTemp;
    GUIDEMO_ShowIntro("Memory devices",
        "For flicker free animation"
        "\nand other purposes");
    GUIDEMO_NotifyStartNext();
    GUI_SetBkColor(GUI_GREEN);
    GUI_Clear();
    /* Use banding memory device  */
    GUI_GetClientRect(&rView);
    colorBack = GUI_ColorIsAvailable(GUI_RED) ? GUI_RED : GUI_BLACK;
    GUI_SetBkColor(colorBack);
    GUI_Clear();
    t0 = GUI_GetTime();
    for (i = 0; tDiff = GUI_GetTime() - t0, (tDiff < 8000) && !GUIDEMO_CheckCancel(); i++) {
        DrawContext.Angle = 90 + tDiff / 50;
        /* Calculate required size */
        {
            GUI_HMEM hMem = GUI_MEASDEV_Create();
            GUI_MEASDEV_Select(hMem);
            DrawContext.DoClear = 0;
            _Draw(&DrawContext);
            GUI_MEASDEV_GetRect(hMem, &rView);
            GUI_MEASDEV_Delete(hMem);
            rTemp = rView;
            if (i)
                GUI_MergeRect(&rView, &rView, &rPrev);
            rPrev = rTemp;
        }
        DrawContext.DoClear = 1;
        GUI_MEMDEV_Draw(&rView, _Draw, &DrawContext, 0, GUI_MEMDEV_NOTRANS);
    }
    GUI_SetFont(&GUI_FontComic24B_ASCII);
    GUI_SetColor(GUI_WHITE);
    GUI_DispStringAt("ms/Update: ", 10, 200);
    if (i) {
        GUI_DispDecMin(tDiff / i);
    }
    GUIDEMO_Delay(1000);
    GUI_Clear();
}

#else

void GUIDEMO_ShowMemoryDevice(void) {}

#endif /* GUI_SUPPORT_MEMDEV */



