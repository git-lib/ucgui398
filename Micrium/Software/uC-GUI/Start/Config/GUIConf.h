﻿/*
*********************************************************************************************************
*                                             uC/GUI V3.98
*                        Universal graphic software for embedded applications
*
*                       (c) Copyright 2002, Micrium Inc., Weston, FL
*                       (c) Copyright 2002, SEGGER Microcontroller Systeme GmbH
*
*              uc/GUI is protected by international copyright laws. Knowledge of the
*              source code may not be used to write a similar product. This file may
*              only be used in accordance with a license and should not be redistributed
*              in any way. We appreciate your understanding and fairness.
*
----------------------------------------------------------------------
File        : GUIConf.h
Purpose     : Configures abilities, fonts etc.
----------------------------------------------------------------------
*/


#ifndef GUICONF_H
#define GUICONF_H

#define GUI_OS                    (1)  //!< 编译器操作系统支持
#define GUI_SUPPORT_TOUCH         (1)  //!< 触摸屏设备支持(需要窗口管理)
#define GUI_SUPPORT_MOUSE         (1)  //!< 鼠标设备支持
#define GUI_SUPPORT_UNICODE       (1)  //!< 支持混合编码的ASCII/UNICODE字符串

#define GUI_DEFAULT_FONT          (&GUI_Font6x8)    //!< 默认字体
#define GUI_ALLOC_SIZE            (12500)  //!< 动态分配内存总大小

#define GUI_DEBUG_LEVEL   (3) 

#define GUI_NUM_LAYERS            (1)   //!< 分层显示层数

// **************************************************************************
// 软件包配置

#define GUI_WINSUPPORT            (1)  //!< 窗口管理支持
#define GUI_SUPPORT_MEMDEV        (1)  //!< 内存设备支持
#define GUI_SUPPORT_AA            (1)  //!< 抗锯齿支持

#endif  /* Avoid multiple inclusion */


	 	 			 		    	 				 	  			   	 	 	 	 	 	  	  	      	   		 	 	 		  		  	 		 	  	  			     			       	   	 			  		    	 	     	 				  	 					 	 			   	  	  			 				 		 	 	 			     			 
