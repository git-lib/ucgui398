﻿/*
*********************************************************************************************************
*                                                uc/GUI
*                        Universal graphic software for embedded applications
*
*                       (c) Copyright 2002, Micrium Inc., Weston, FL
*                       (c) Copyright 2000, SEGGER Microcontroller Systeme GmbH
*
*              uc/GUI is protected by international copyright laws. Knowledge of the
*              source code may not be used to write a similar product. This file may
*              only be used in accordance with a license and should not be redistributed
*              in any way. We appreciate your understanding and fairness.
*
* File        : MainTask.c
* Purpose     : Application program in windows simulator
*********************************************************************************************************
*/


#include "GUI.h"

// **************************************************************************
// 外部引用变量

extern const GUI_BITMAP bmMicriumLogo;      //!< logo位图
extern const GUI_BITMAP bmMicriumLogo_1bpp; //!< logo单色位图

// **************************************************************************
// 常量定义

const int DELAY_TIME = 2000;

// **************************************************************************
// 接口函数

//! \brief      main demo主函数
//! \details    该函数为GUI默认的主函数
//! \return     void
void MainTask(void) {
    int Cnt = 0;
    int i, YPos;
    //获取LCD x,y方向大小
    int LCDXSize = LCD_GET_XSIZE();
    int LCDYSize = LCD_GET_YSIZE();
    const GUI_BITMAP *pBitmap;
    //初始化GUI并顺序显示3中颜色测试
    GUI_Init();
    GUI_SetBkColor(GUI_RED);
    GUI_Clear();
    GUI_Delay(DELAY_TIME);
    GUI_SetBkColor(GUI_BLUE);
    GUI_Clear();
    GUI_Delay(DELAY_TIME);
    GUI_SetColor(GUI_WHITE);
    for (i = 0; i < 1000; i += 10) {
        GUI_DrawHLine(i, 0, 100);
        GUI_DispStringAt("Line ", 0, i);
        GUI_DispDecMin(i);
    }
    GUI_Delay(DELAY_TIME);
    GUI_SetColor(0x0);
    GUI_SetBkColor(0xffffff);
    for (i = 0; i < 160; i++) {
        int len = (i < 80) ? i : 160 - i;
        GUI_DrawHLine(i, 20, len + 20);
    }
    GUI_Delay(DELAY_TIME);
    GUI_Clear();
    if (LCD_GET_YSIZE() > (100 + bmMicriumLogo_1bpp.YSize)) {
        pBitmap = &bmMicriumLogo;
    }
    else {
        GUI_SetColor(GUI_BLUE);
        pBitmap = &bmMicriumLogo_1bpp;
    }
    GUI_DrawBitmap(pBitmap, (LCDXSize - pBitmap->XSize) / 2, 10);
    YPos = 20 + pBitmap->YSize;
    GUI_SetFont(&GUI_FontComic24B_1);
    GUI_DispStringHCenterAt("www.micrium.com", LCDXSize / 2, YPos);
    GUI_Delay(DELAY_TIME);
    GUI_SetColor(GUI_RED);
    GUI_DispStringHCenterAt("?2005\n", LCDXSize / 2, YPos + 30);
    GUI_SetFont(&GUI_Font16_1);
    GUI_DispStringHCenterAt("Micrium Inc.", LCDXSize / 2, YPos + 60);;
    GUI_Delay(DELAY_TIME);
}

