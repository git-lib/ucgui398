/*
*********************************************************************************************************
*                                                uC/GUI
*                        Universal graphic software for embedded applications
*
*                       (c) Copyright 2002, Micrium Inc., Weston, FL
*                       (c) Copyright 2002, SEGGER Microcontroller Systeme GmbH
*
*              uc/GUI is protected by international copyright laws. Knowledge of the
*              source code may not be used to write a similar product. This file may
*              only be used in accordance with a license and should not be redistributed
*              in any way. We appreciate your understanding and fairness.
*
----------------------------------------------------------------------
File        : SIM.h
Purpose     : Declares public functions of Simulation
----------------------------------------------------------------------
*/

#ifndef SIM_H
#define SIM_H


// **************************************************************************
// 硬件按键模拟

typedef void SIM_HARDKEY_CB(int KeyIndex, int State);

int              SIM_HARDKEY_GetNum(void);
int              SIM_HARDKEY_GetState(unsigned int i);
SIM_HARDKEY_CB*  SIM_HARDKEY_SetCallback(unsigned int KeyIndex, SIM_HARDKEY_CB* pfCallback);
int              SIM_HARDKEY_SetMode (unsigned int KeyIndex, int Mode);
int              SIM_HARDKEY_SetState(unsigned int KeyIndex, int State);


// **************************************************************************
// LCD公共方法

void     SIM_SetLCDPos(int x, int y);
int      SIM_SetTransColor(int Color);
int      SIM_SetLCDColorBlack (unsigned int Index, int Color);
int      SIM_SetLCDColorWhite (unsigned int Index, int Color);
void     SIM_SetMag(int MagX, int MagY);
int      SIM_GetMagX(void);
int      SIM_GetMagY(void);


// **************************************************************************
// 用户初底层始化例程

void  SIM_X_Init(void);


// **************************************************************************
// GUI_X公用模块

void SIM_Delay (int ms);
void SIM_ExecIdle(void);
int  SIM_GetTime(void);
int  SIM_GetKey(void);
int  SIM_WaitKey(void);
void SIM_StoreKey(int);


// **************************************************************************
// 日志、警告和错误输出的公共方法

void SIM_Log(const char *s);

void SIM_Warn(const char *s);
void SIM_ErrorOut(const char *s);
void SIM_EnableMessageBoxOnError(int Status);

// **************************************************************************
// 命令行支持

const char *SIM_GetCmdLine(void);


// **************************************************************************
// 多任务支持

void SIM_CreateTask(char * pName, void * pFunc);
void SIM_Start(void);
unsigned long SIM_GetTaskID(void);
void SIM_Lock(void);
void SIM_Unlock(void);
void SIM_InitOS(void);

#endif /* LCD_H */




